export async function fetchProducts(page = 1) {
  try {
    const response = await fetch(
      "http://localhost:3000/api/products?page=" + page
    );
    const body = await response.json();
    return body;
  } catch (error) {
    console.log("Error", error.message);
    return { error: error.message };
  }
}

export async function fetchProductByUUID(uuid) {
  try {
    const response = await fetch(
      "http://localhost:3000/api/products?page=1&uuid=" + uuid
    );
    const body = await response.json();
    return body.results[0];
  } catch (error) {
    console.log("Error", error.message);
    return { error: error.message };
  }
}
