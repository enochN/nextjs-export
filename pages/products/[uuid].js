import Link from "next/link";
import Head from "next/head";
import { fetchProductByUUID, fetchProducts } from "../../utils/api";

export async function getStaticPaths() {
  const response = await fetchProducts(1);
  const paths = response?.results?.map((post) => {
    return {
      params: {
        uuid: `${post.uuid}`,
      },
    };
  });

  return {
    paths: paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  // fetch single post detail
  const product = await fetchProductByUUID(params.uuid);
  return {
    props: product,
  };
}

export default function Post({ name, display_name, brand_name }) {
  return (
    <main>
      <Head>
        <title>{name}</title>
      </Head>

      <h1>{display_name}</h1>

      <p>This is {brand_name}</p>

      <Link href="/">
        <a>Go back to home</a>
      </Link>
    </main>
  );
}
