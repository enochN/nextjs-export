// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default async function handler(req, res) {
  const { page, page_size } = req.body;
  const response = await fetch(
    `https://api-test.mpharma.com/inventory-bff/productaliases/?page=${page}&page_size=${page_size}&facility_id=5e5e3ba59c070e004ee55184`
  );

  const body = await response.json();
  res.status(200).json({ ...body });
}
