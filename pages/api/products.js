// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import axios from "axios";

export default async function handler(req, res) {
  const { page, uuid } = req.query;
  const response = await axios.get(
    `https://api-test.mpharma.com/products-microservice/productaliases/?page=${page}&page_size=50&facility_id=5e5e3ba59c070e004ee55184${
      Boolean(uuid) ? `&uuid=${uuid}` : ""
    }`,
    {
      headers: {
        Authorization: `Bearer Uada15532-045e-40ee-82d6-f03ef137fc20`,
      },
    }
  );

  const body = response.data;
  res.status(200).json({ ...body });
}
